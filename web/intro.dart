/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

part of manta;

///////////////////////////////////////////////

class CIntroState extends CState
{

  CTransform mTransform;
  CNode      mRoot, mPlane, mLogo, mRunway;
  Vector3    mOrigin;
  double     mAngle;

  CIntroState()
  {
    print("Doing Introduction");
    mTransform = new CTransform();
    mTransform.setProjection3D();
    mAngle = 0.0;
    mOrigin = new Vector3(0.0, 1.0, 0.0);

    mRoot = new CNode();
    mRoot.setForRoot(mTransform);

    mPlane = mRoot.addChild();
    mPlane.mScale.setValues(1000.0, 1.0, 1000.0);
    mPlane.mRenderable = Art.plane;
    mPlane.mTint.setFrom(Colours.get('LimeGreen'));

    mLogo = mRoot.addChild();
    mLogo.mPosition.setValues(0.0, 0.1, 0.0);
    mLogo.mRenderable = Art.logo;

    mRunway = mRoot.addChild();
    mRunway.mRenderable = Art.runway;
    mRunway.mPosition.setValues(-400.0, 0.02, 0.0);

  }

  void stop()
  {

  }

  void tick(double deltaTime)
  {
    gl.clearColor(0.4, 0.49, 0.7337, 1.0);
    gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

    mAngle += 45.0 * Game.mDeltaTime;
    mTransform.setCameraOrbitCosSin(mOrigin, mAngle, 10.0, 4.0);
    //mRoot.draw();

    GUI.drawText(97, 170, 1, "Press [S] to Continue");
    GUI.drawText(91, 190, 1, "(c) Robin Southern 2013");

    //if (Game.isKeyReleased(KeyCode.S))
    //{
      Game.pushState(new CFlightState());
    //}
  }

}

///////////////////////////////////////////////
